import { Injectable } from '@angular/core';
import {Observable, of, throwError} from 'rxjs';
import {Movie} from './models/movie';
import {map} from 'rxjs/operators';

const MOVIES: Movie[] = [
  { id: 1, title: 'Bad Boys for Life', genres: ['Action', 'Comedy'], poster: 'https://media.s-bol.com/76v2A2EV56Pr/550x819.jpg'},
  { id: 2, title: 'Bloodshot', genres: ['Action', 'Adventure'], poster: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcSk-Xjs0S4KVnLByZNpAT-oaFmzlcS8xVzJL9sGTXUIRw7s2aMh'},
  { id: 3, title: 'Enola Holmes', genres: ['Crime', 'Adventure'], poster: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcSudXMPDdUNZJXa_MF38nFic5bSd_DunfwoKw81xapTFKpTIOYZ'},
  { id: 4, title: 'Fantasy Island', genres: ['Horror', 'Thriller'], poster: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcSTujjZnnnu_LhRSBrE0TY7XBW9OOtbDK8zsB7EAvw6k9GIlS0a'},
  { id: 5, title: 'Gretel & Hansel', genres: ['Horror', 'Fantasy'], poster: 'https://encrypted-tbn3.gstatic.com/images?q=tbn:ANd9GcR52ycmqF59sH9UNMX5ZNrMtI3I-XmwFQeCD9ErU3EZC26QwUdd'},
  { id: 6, title: 'Onward', genres: ['Family', 'Animation'], poster: 'https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcR5dj5nsOPEbkyr_Yn2TH5Aj7s4y0PazNrvx7HUJYoFa7KQZqqE'},
  { id: 7, title: 'Papillon', genres: [ 'Adventure', 'Crime' ], poster: 'https://upload.wikimedia.org/wikipedia/en/4/4e/Papillon_2018_poster.png'},
  { id: 8, title: 'Red Sparrow', genres: [ 'Thriller' ], poster: 'https://m.media-amazon.com/images/M/MV5BMTA3MDkxOTc4NDdeQTJeQWpwZ15BbWU4MDAxNzgyNTQz._V1_.jpg'},
  { id: 9, title: 'The Call of the Wild', genres: ['Adventure', 'Drama'], poster: 'https://encrypted-tbn1.gstatic.com/images?q=tbn:ANd9GcTdNXjm_ExhR2bDuz5Ai54d-EH6jtmrcbhRZDa66SEcEkwaXovX'},
  { id: 10, title: 'The Old Guard', genres: [ 'Action', 'Fantasy' ], poster: 'https://encrypted-tbn2.gstatic.com/images?q=tbn:ANd9GcT2z3g2RjEekVFn-HAm9Ro6EtlRjU-7OWxPvMq_tSYKd7JyOEa6'},
  { id: 11, title: 'Underwater', genres: [ 'Horror', 'Thriller' ], poster: 'https://m.media-amazon.com/images/M/MV5BNzM0OGZiZWItYmZiNC00NDgzLTg1MjMtYjM4MWZhOGZhMDUwXkEyXkFqcGdeQXVyMTkxNjUyNQ@@._V1_.jpg'},
  { id: 12, title: 'Venom', genres: [ 'Action', 'Sci-fi' ], poster: 'https://images-na.ssl-images-amazon.com/images/I/818KhZOyaTL._AC_SY679_.jpg'},
];

@Injectable({
  providedIn: 'root'
})
export class MoviesService {
  getMoviesSync(): Movie[] {
    return MOVIES.map(m => ({...m}));
  }

  getMovie(id: number): Observable<Movie> {
    return of(MOVIES).pipe(map(movies => {
      const movie = movies.find(m => m.id === id);

      if (!movie) {
        throw throwError('Movie not found.');
      }

      return movie;
    }));
  }
}
