export class Movie {
  id: number;
  title: string;
  genres: string[];
  poster: string;
}
