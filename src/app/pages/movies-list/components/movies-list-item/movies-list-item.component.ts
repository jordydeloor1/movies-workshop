import {Component, Input} from '@angular/core';
import {Movie} from '../../../../services/movies/models/movie';

@Component({
  selector: 'app-movies-list-item',
  templateUrl: './movies-list-item.component.html',
  styleUrls: ['./movies-list-item.component.scss']
})
export class MoviesListItemComponent {
  @Input()
  movie: Movie;
}
