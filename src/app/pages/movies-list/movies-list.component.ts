import { Component, OnInit } from '@angular/core';
import {Movie} from '../../services/movies/models/movie';
import {MoviesService} from '../../services/movies/movies.service';

@Component({
  selector: 'app-movies-list',
  templateUrl: './movies-list.component.html',
  styleUrls: ['./movies-list.component.scss']
})
export class MoviesListComponent implements OnInit {
  movies: Movie[];

  constructor(private moviesService: MoviesService) {}

  ngOnInit() {
    this.movies = this.moviesService.getMoviesSync();
  }
}
