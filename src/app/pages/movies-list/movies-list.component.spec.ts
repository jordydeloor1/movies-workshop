import { MoviesListComponent } from './movies-list.component';
import { Movie } from '../../services/movies/models/movie';
import createSpy = jasmine.createSpy;
import createSpyObj = jasmine.createSpyObj;

function generateMovie(id: number): Movie {
  return {
    id,
    title: `Movie${id}`,
    genres: ['Action'],
    poster: ''
  };
}

describe('MoviesListComponent', () => {
  it('should initialize movies', () => {
    const movie1 = generateMovie(1);
    const movie2 = generateMovie(2);

    const movies = [movie1, movie2];

    const moviesService = createSpyObj(['getMoviesSync']);
    moviesService.getMoviesSync.and.returnValue(movies);

    const component = new MoviesListComponent(moviesService);

    component.ngOnInit();

    expect(component.movies).toBe(movies);
    expect(component.movies[0].id).toBe(1);
    expect(moviesService.getMoviesSync).toHaveBeenCalled();
  });
});
