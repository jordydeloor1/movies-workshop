import { Component, OnInit } from '@angular/core';
import {ActivatedRoute, Router} from '@angular/router';
import {Movie} from '../../services/movies/models/movie';
import {switchMap} from 'rxjs/operators';
import {MoviesService} from '../../services/movies/movies.service';

@Component({
  selector: 'app-movie-details',
  templateUrl: './movie-details.component.html',
  styleUrls: ['./movie-details.component.scss']
})
export class MovieDetailsComponent implements OnInit {
  movie: Movie;

  constructor(private moviesService: MoviesService, private route: ActivatedRoute, private router: Router) {}

  ngOnInit() {
    this.route.paramMap
      .pipe(switchMap( params => {
        const id = +params.get('id');
        return this.moviesService.getMovie(id);
      }))
      .subscribe(
        movie => {
          this.movie = movie;
        },
        () => {
          this.router.navigate(['/']);
        }
      );
  }
}
