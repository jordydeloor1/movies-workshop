import { MovieDetailsComponent } from './movie-details.component';
import createSpyObj = jasmine.createSpyObj;
import {of, throwError, timer} from 'rxjs';
import {mapTo} from 'rxjs/operators';
import {Movie} from '../../services/movies/models/movie';
import {fakeAsync, tick} from '@angular/core/testing';

function generateMovie(id: number): Movie {
  return {
    id,
    title: `Movie${id}`,
    genres: ['Action'],
    poster: ''
  };
}

describe('MovieDetailsComponent', () => {
  it('should get movie', fakeAsync(() => {
    const moviesService = createSpyObj(['getMovie']);
    const params = createSpyObj(['get']);
    const route: any = {
      paramMap: of(params)
    };
    const router = createSpyObj(['navigate']);
    const movie = generateMovie(1);

    moviesService.getMovie.and.returnValue(timer(100).pipe(mapTo(movie)));

    const component = new MovieDetailsComponent(moviesService, route, router);

    component.ngOnInit();
    tick(100);

    expect(component.movie).toEqual(movie);
  }));

  it('should not get movie', () => {
    const moviesService = createSpyObj(['getMovie']);
    const params = createSpyObj(['get']);
    const route: any = {
      paramMap: of(params)
    };
    const router = createSpyObj(['navigate']);

    moviesService.getMovie.and.returnValue(throwError('Movie not found'));

    const component = new MovieDetailsComponent(moviesService, route, router);

    component.ngOnInit();

    expect(component.movie).toBeUndefined();
    expect(router.navigate).toHaveBeenCalled();
  });
});
