import { AppComponent } from './app.component';
import {TestBed} from '@angular/core/testing';
import {NO_ERRORS_SCHEMA} from '@angular/core';
import {By} from '@angular/platform-browser';

describe('AppComponent', () => {
  it('should initialize title', () => {
    const component = new AppComponent();

    component.ngOnInit();

    expect(component.title).toBe('Movies');
  });

  it('should set title', () => {
    TestBed.configureTestingModule({
      declarations: [AppComponent],
      providers: [],
      schemas: [NO_ERRORS_SCHEMA],
    }).compileComponents();

    const fixture = TestBed.createComponent(AppComponent);
    const component = fixture.componentInstance;

    component.ngOnInit();

    fixture.detectChanges();

    const h1 = fixture.debugElement.query(By.css('h1'));
    expect(h1.nativeElement.innerHTML).toBe('Movies');
  });
});
